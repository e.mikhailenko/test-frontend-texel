import React from 'react';
import {useQuery} from "react-apollo";
import ServerDescription from "./serverDescription";
import ServerForm from "./serverForm";
import gql from "graphql-tag";

const queryServer = gql`
    query {
        server(id: 1) {
            name
            title
            description
            parameters {
                input {
                    type
                    name
                    title
                    items {
                        value
                        title
                    }
                }
                output {
                    type
                    name
                    title
                }
            }
            commands {
                start
            }
        }
    }
`

function Server() {

    const {loading, error, data} = useQuery(queryServer);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
        <>
            <ServerDescription server={data?.server[0]}/>
            <ServerForm dataInputs={data?.server[0].parameters[0]?.input}/>
        </>
    )
}

export default Server
