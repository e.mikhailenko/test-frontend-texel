import React from "react";
import {
    chakra,
    Flex,
    HStack,
    Icon,
    IconButton,
    Link,
    useColorMode,
    useColorModeValue,
    useDisclosure,
    CloseButton,
    Box,
    VStack,
    Button,
} from "@chakra-ui/react";
import {useViewportScroll} from "framer-motion";
import {FaMoon, FaSun, FaHeart} from "react-icons/fa";
import {
    AiOutlineMenu,
    AiFillHome,
    AiOutlineInbox,
} from "react-icons/ai";
import {BsFillCameraVideoFill} from "react-icons/bs";
import {Logo} from "@choc-ui/logo";

const ChakraUIHeader = () => {
    const mobileNav = useDisclosure();

    const {toggleColorMode: toggleMode} = useColorMode();
    const text = useColorModeValue("dark", "light");
    const SwitchIcon = useColorModeValue(FaMoon, FaSun);

    const bg = useColorModeValue("white", "gray.800");
    const {current} = React.useRef();
    const [y, setY] = React.useState(0);
    let height: number;
    if (current) {
        // @ts-ignore
        ({height = 0} = current.getBoundingClientRect());
    } else {
        ({height = 0} = {});
    }

    const {scrollY} = useViewportScroll();
    React.useEffect(() => {
        return scrollY.onChange(() => setY(scrollY.get()));
    }, [scrollY]);
    const MobileNavContent = (
        <VStack
            pos="absolute"
            top={0}
            left={0}
            right={0}
            display={mobileNav.isOpen ? "flex" : "none"}
            flexDirection="column"
            p={2}
            pb={4}
            m={2}
            bg={bg}
            spacing={3}
            rounded="sm"
            shadow="sm"
        >
            <CloseButton
                aria-label="Close menu"
                justifySelf="self-start"
                onClick={mobileNav.onClose}
            />
            <Button w="full" variant="ghost" leftIcon={<AiFillHome/>}>
                Dashboard
            </Button>
            <Button
                w="full"
                variant="solid"
                colorScheme="brand"

                leftIcon={<AiOutlineInbox/>}
            >
                Inbox
            </Button>
            <Button
                w="full"
                variant="ghost"

                leftIcon={<BsFillCameraVideoFill/>}
            >
                Videos
            </Button>
        </VStack>
    );
    return (
        <Box pos="relative">
            <chakra.header
                // ref={ref}
                shadow={y > height ? "sm" : undefined}
                transition="box-shadow 0.2s"
                bg={bg}
                borderTop="6px solid"
                borderTopColor="brand.400"
                w="full"
                overflowY="hidden"
            >
                <chakra.div h="4.5rem" mx="auto" maxW="1200px">
                    <Flex w="full" h="full" px="6" align="center" justify="space-between">
                        <Flex align="center">
                            <Link href="/">
                                <HStack>
                                    <Logo/>
                                </HStack>
                            </Link>
                        </Flex>

                        <Flex
                            justify="flex-end"
                            w="full"
                            maxW="824px"
                            align="center"
                            color="gray.400"
                        >
                            <HStack spacing="5" display={{base: "none", md: "flex"}}>
                            </HStack>
                            <IconButton
                                size="md"
                                fontSize="lg"
                                aria-label={`Switch to ${text} mode`}
                                variant="ghost"
                                color="current"
                                ml={{base: "0", md: "3"}}
                                onClick={toggleMode}
                                icon={<SwitchIcon/>}
                            />
                            <IconButton
                                display={{base: "flex", md: "none"}}
                                aria-label="Open menu"
                                fontSize="20px"
                                color={useColorModeValue("gray.800", "inherit")}
                                variant="ghost"
                                icon={<AiOutlineMenu/>}
                                onClick={mobileNav.onOpen}
                            />
                        </Flex>
                    </Flex>
                    {MobileNavContent}
                </chakra.div>
            </chakra.header>
        </Box>
    );
};

export default ChakraUIHeader;
