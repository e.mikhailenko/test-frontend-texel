import React from "react";
import {
    chakra,
    Box,
    Flex,
    useColorModeValue,
    Link,
} from "@chakra-ui/react";

// @ts-ignore
const ServerDescription = ({server}) => {
    return (
        <Flex
            bg={useColorModeValue("#F9FAFB", "gray.600")}
            p={10}
            alignItems="center"
            justifyContent="center"
        >
            <Box
                mx="auto"
                px={8}
                py={4}
                rounded="lg"
                shadow="lg"
                bg={useColorModeValue("white", "gray.800")}
                maxW="2xl"
                w="2xl"
            >
                <Box mt={2}>
                    <Link
                        fontSize="2xl"
                        color={useColorModeValue("gray.700", "white")}
                        fontWeight="700"
                        _hover={{
                            color: useColorModeValue("gray.600", "gray.200"),
                            textDecor: "underline",
                        }}
                    >
                        {server.name}
                    </Link>
                    <chakra.p
                        mt={2}
                        color={useColorModeValue("gray.600", "gray.300")}
                        fontSize="xl"
                    >
                        {server.title}
                    </chakra.p>
                    <chakra.p
                        mt={2}
                        color={useColorModeValue("gray.600", "gray.300")}
                        fontSize="xl"
                    >
                        {server.description}
                    </chakra.p>
                </Box>
            </Box>
        </Flex>
    );
};

export default ServerDescription;
