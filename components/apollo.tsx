import ApolloClient from "apollo-boost";
import {ApolloProvider} from "@apollo/react-common";
import Server from "./server";
import React from "react";

let headers = new Headers();

headers.append('Content-Type', 'application/json');
headers.append('Accept', 'application/json');
headers.append('Access-Control-Allow-Origin', '*');
headers.append('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
headers.append('Origin', 'http://localhost:3000');

const client = new ApolloClient({
    uri: 'http://localhost:8000/graphql/',
    headers: headers
});


const App = () => (
    <ApolloProvider client={client}>
        <Server/>
    </ApolloProvider>
);

export default App;
