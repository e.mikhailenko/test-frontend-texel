import {
    Box,
    chakra,
    FormControl,
    FormLabel,
    GridItem,
    Input,
    Select,
    SimpleGrid,
    Stack,
    useColorModeValue
} from "@chakra-ui/react";
import React, {useState} from "react";
import SubmitButton from "./submitButton";

// @ts-ignore
export default function ServerForm({dataInputs}) {

    const [inputText, setInputText] = useState<string | null>(null);
    const [inputNumber, setInputNumber] = useState<number | null>(null);

    return (
        <>
            <Box bg={useColorModeValue("gray.50", "inherit")} p={5}>
                <Box visibility={{base: "hidden", sm: "visible"}} aria-hidden="true">
                    <Box py={5}>
                        <Box
                            borderTop="solid 1px"
                            borderTopColor={useColorModeValue("gray.200", "whiteAlpha.200")}
                        />
                    </Box>
                </Box>
                <Box mt={[10, 0]}>
                    <SimpleGrid
                        display={{base: "initial", md: "grid"}}
                        columns={{md: 2}}
                        spacing={{md: 6}}
                    >
                            <GridItem mt={[5, null, 0]} colSpan={{md: 2}}>
                                <Stack
                                    px={4}
                                    py={5}
                                    p={[null, 6]}
                                    bg={useColorModeValue("white", "gray.700")}
                                    spacing={6}
                                >
                                    {dataInputs?.map((dataInput: any, index: React.Key | null | undefined) => (
                                        <Box key={index}>
                                            {dataInput?.type === "number" &&
                                            <FormControl
                                                isRequired
                                                as={GridItem}
                                                colSpan={[6, 4]}
                                            >
                                                <FormLabel
                                                    fontSize="lg"
                                                    fontWeight="md"
                                                    /* eslint-disable-next-line react-hooks/rules-of-hooks */
                                                    color={useColorModeValue("gray.700", "gray.50")}
                                                >
                                                    Input Number
                                                </FormLabel>
                                                <Input
                                                    type="number"
                                                    name="input_num"
                                                    id="input_num"
                                                    autoComplete="input-num"
                                                    mt={1}
                                                    focusBorderColor="brand.400"
                                                    shadow="sm"
                                                    size="lg"
                                                    w="full"
                                                    rounded="md"
                                                    onChange={
                                                        // @ts-ignore
                                                        (e) => setInputNumber(e.target.value)
                                                    }
                                                />
                                            </FormControl>
                                            }
                                            {dataInput?.type === "select" &&
                                            <FormControl
                                                isRequired
                                                as={GridItem}
                                                colSpan={[6, 3]}
                                            >
                                                <FormLabel
                                                    htmlFor="input_text"
                                                    fontSize="lg"
                                                    fontWeight="md"
                                                    /* eslint-disable-next-line react-hooks/rules-of-hooks */
                                                    color={useColorModeValue("gray.700", "gray.50")}
                                                >
                                                    Input Text
                                                </FormLabel>
                                                <Select
                                                    id="input_text"
                                                    name="input_text"
                                                    autoComplete="input_text"
                                                    placeholder="Select option"
                                                    mt={1}
                                                    focusBorderColor="brand.400"
                                                    shadow="sm"
                                                    size="lg"
                                                    w="full"
                                                    rounded="md"
                                                    onChange={(e) => {
                                                        // @ts-ignore
                                                        setInputText(e.target.value);
                                                    }}
                                                >
                                                    {dataInput?.items?.map((
                                                        item: { value: any },
                                                        index: React.Key | null | undefined) =>
                                                        <option key={index}>{item?.value}</option>
                                                    )}
                                                </Select>
                                            </FormControl>
                                            }
                                        </Box>
                                    ))}
                                </Stack>
                                <SubmitButton inputNumber={inputNumber} inputText={inputText}/>
                            </GridItem>
                    </SimpleGrid>
                </Box>
            </Box>
        </>
    );
}
