import {Box, Button, useColorModeValue} from "@chakra-ui/react";
import React, {useState} from "react";
import gql from "graphql-tag";
import {useMutation} from "react-apollo";

const mutationUpdateOutput = gql`
    mutation UpdateOutput($inputNumber: Int, $inputText: String) {
        updateOutput(inputNum: $inputNumber, inputText: $inputText) {
            output {
                value
            }
        }
    }
`

// @ts-ignore
export default function SubmitButton({inputNumber, inputText}) {

    const [outputData, setOutputData] = useState(null);
    const [updateOutput, {data}] = useMutation(mutationUpdateOutput);

    return (
        <Box
            px={{base: 4, sm: 6}}
            py={3}
            bg={useColorModeValue("gray.100", "gray.900")}
            textAlign="right"
            roundedBottom="lg"
        >
            <Button
                _focus={{shadow: ""}}
                fontWeight={"lg"}
                fontSize={"lg"}
                color={useColorModeValue("gray.500", "gray.1000")}
                onClick={(e) => {
                    updateOutput({variables: {inputNumber: inputNumber, inputText: inputText}}).then();
                    setOutputData(data?.updateOutput?.output.value);
                }}
            >
                Send Data
            </Button>
            <Box>{outputData}</Box>
        </Box>
    );
}
