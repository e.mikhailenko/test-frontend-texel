import Header from "../components/header";
import Apollo from "../components/apollo";
import React from "react";


export default function Home() {
    return (
        <>
            <Header/>
            <Apollo/>
        </>
    )
}


